#pragma once
#include <variant>
#include "typelist_macro.h"

using variant_t = std::variant
<
std::monostate
TYPE_LIST
>;

class variant : public variant_t {
public:

    TYPE AS { return std::get<TYPE>(*this); }
};
